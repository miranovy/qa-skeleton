<?php declare(strict_types = 1);

namespace App\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FooCommand extends Command
{

    /** @var string */
    protected static $defaultName = 'foo';

    protected function configure(): void
    {
        $this->setDescription('Foo command')
            ->setHelp('This command help');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('I am foo command!');

        return 0;
    }

}
