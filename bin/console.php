#!/usr/bin/env php
<?php declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use App\Console\FooCommand;

$application = new Application();

// register commands
$application->add(new FooCommand());

$application->run();
