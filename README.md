# QA skeleton
Example of php QA skeleton.

## Commands

To run `lint` use command
```bash
make lint
```

To run `codesniffer` use command
```bash
make cs
```

To run `codefixer` use command
```bash
make csf
```

To run `phpstan` use command
```bash
make phpstan
```

To run all together (codesniffer, lint and phpstan) use command.
```bash
make qa
```
Note: This command is used by git precommit hook.



